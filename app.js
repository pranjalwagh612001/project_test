const express = require('express');
const app = express();
const port = 3000;

app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send(`
    <style>
      body {
        background-color: #f0f0f0; /* Light grey background */
        font-family: Arial, sans-serif;
      }
      form {
        background-color: #fff;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        width: 300px;
        margin: 0 auto;
      }
      label {
        display: block;
        margin-bottom: 10px;
      }
      input {
        width: 100%;
        padding: 8px;
        margin-bottom: 15px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
      }
      button {
        background-color: #4caf50; /* Green */
        color: white;
        padding: 10px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }
      button:hover {
        background-color: #45a049;
      }
    </style>
    <h1>Welcome to My Web App</h1>
    <form method="post" action="/submit">
      <label for="name">Name:</label>
      <input type="text" id="name" name="name" required><br>
      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required><br>
      <label for="phone">Phone:</label>
      <input type="tel" id="phone" name="phone"><br>
      <label for="address">Address:</label>
      <input type="text" id="address" name="address"><br>
      <button type="submit">Submit</button>
    </form>
  `);
});

app.post('/submit', (req, res) => {
  const { name, email, phone, address } = req.body;
  
  res.send(`Thank you, ${name}, for submitting your details:<br>
            Email: ${email}<br>
            Phone: ${phone}<br>
            Address: ${address}`);
});

app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});
